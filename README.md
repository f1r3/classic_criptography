# classic_criptography

This is a series of classic cryptography algorithms.

All the scripts will be integrated on cripto.py (already integrated some algorithms)

Developed in python 3 using scripting.

Initially, the following 5 algorithms will be developed:<br>
hebrew atbash <strong> (Done) </strong><br>
vigenere algorithm <strong> (Done) </strong><br>
vernam algorithm<strong>(Done)</strong><br>
cryptoalalysis kasiski<strong>(Done)</strong><br>
enigma machine<br>

But it's intended to develop the following algorithms:<br>
reverse transposition, simple and double<br>
transposition by groups<br>
tranposition by series<br>
rotating mask<br>
playfair algorithm<br>
cesar algorithm<br>
adfgvx algorithm<br>
algorithm of vigenere (autoclave)<br>
algorithm of vigenere (rozier)<br>
algorithm of vigenere (beaufort)<br>
polybios algorithm<br>
affine encryption<br>
cryptoanalysis kasiski<br>
frequency cryptanalysis (cesar)<br>
hagelin machine<br>
colossus machine<br>

Usage instructions will be placed here, and the algorithms that are already developed in the script will be marked.

Implemented algorithms<br>
hebrew atbash<br>
vigenere algorithm<br>