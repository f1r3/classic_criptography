#!/usr/bin/python3
import argparse
import os
import os.path as path

def encrypt(m,k):
    kl = []
    c = ""
    for ck in k:
#        kl.append(bin(ord(ck)))
        kl.append(ord(ck))
    skl = len(kl)
    print("ascii llaves: {}".format(kl))
    for i,cm in enumerate(m):
        print("{} kl[{}] = {}\t{} cm[] = {} \txOr = {}".format(chr(kl[i%skl]),i,kl[i%skl],cm,ord(cm), ord(cm) ^ kl[i%skl]))
        c += chr(ord(cm) ^ kl[i%skl])

    #print ("longitud de c: {}".format(len(c)))
    #print ("c = {}".format(c))
    return c;

def decrypt(c,k):
    return encrypt(c,k)

def main():
    alphabet = ""
    message = ""
    cryptogram = ""
    key = ""
    parser = argparse.ArgumentParser(description="Vernam cipher: Developed by G.S. Vernam in bell and AT&T labs. Convert message letters to binary(wich can be represented in ASCII code). The early algorithms to Vernam consider the character the smallest unit, Vernam consider the bit the smallest unit. Verman algorithm use a key as long as the message, it converts the message and the key to binary, and apply XOR operation character by character, and finally convert the result to ASCII.  Repository https://gitlab.com/tavomoran/classic_criptography This program has not warranty, use under your own risk. Script developed by Gustavo Morán - Alix Estefany Cabrera")
    inputGroup = parser.add_mutually_exclusive_group(required = True)
    inputGroup.add_argument("-i", "--input", type=str, help="set a message to encrypt with -e flag, or a criptograma to decrypt with the -d flag")
    inputGroup.add_argument("-iF", "--inputFile",type=str, help="set an input file to read the message or the cryptograma, use -e to encrypt or -d to decrypt"); 
    keyGroup = parser.add_mutually_exclusive_group(required = True)
    keyGroup.add_argument("-k", "--key", type=str, help="set a key. NOTE: verman algorithm originally use a key long as the message, but for practial porpuses if the key is smallest it will be repeated to fullfit the message size, and if the key is longest it will be croped to fit the message ")
    keyGroup.add_argument("-kF", "--keyFile", type=str, help="set a file to read the key. NOTE: veerman algorithm originally use a key as long as the message, but for practial porpuses if the key is smalles it will be repeated to fullfit the message size, and if the key is longes it will be croped to fit the message")
    encDecGroup = parser.add_mutually_exclusive_group(required=True)
    encDecGroup.add_argument("-e", "--encrypt", action="store_true", help="to encrypt the message")
    encDecGroup.add_argument("-d","--decrypt", action="store_true", help="to decrypt the cryptogram")
    outputGroup = parser.add_mutually_exclusive_group()
    outputGroup.add_argument("-o", "--output", type=str, help="set the output file name. WARNING: this will overwrite the file")

    args = parser.parse_args()
    
    if args.key:
        key = args.key
    elif args.keyFile:
        if path.exists(args.keyFile):
            f = open(args.keyFile)
            reading = f.read()
            f.close()
            key = reading[0:-1]
        else:
            print("Error: {} file dos not Exist or cannot been Found!".format(args.keyFile))
            return

    if args.input:
        if args.encrypt:
            message = args.input
        elif args.decrypt:
            cryptogram = args.input
    elif args.inputFile:
        reading=""
        if path.isfile(args.inputFile):
            f = open(args.inputFile, encoding="ISO-8859-1")
            reading = f.read()
            f.close()
            if args.encrypt:
                message = reading
            elif args.decrypt:
                cryptogram = reading
        else:
            print("Error: {} file does not Exist or cannot been Found!".format(args.inputFile))
            return
        
    if args.output:
        if path.exists(args.output):
            os.remove(args.output)
        f = open(args.output, "x", encoding="ISO-8859-1")
        if args.encrypt:
            cryptogram = encrypt(message,key)
            f.write(cryptogram)
            print ("message has been encrypted in file {}".format(args.output))
        elif args.decrypt:
            message = decrypt(cryptogram,key)
            f.write(message)
            f.close()
            print ("cryptogram has been decrypted in file {}".format(args.output))
    else:
        if args.encrypt:
            cryptogram = encrypt(message,key)
            print ("message: {}".format(message))
            print ("cryptogram: {}".format(cryptogram))
        elif args.decrypt:
            message = decrypt(cryptogram,key)
            print ("cryptogram: {}".format(cryptogram))
            print ("message: {}".format(message))



if __name__ == '__main__':
    main()
