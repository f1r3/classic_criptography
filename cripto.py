#!/usr/bin/python3
import argparse
import os
import os.path as path
import string
from time import time

#spanish_alphabet = string.printable
spanish_alphabet = "".join([chr(i) for i in range(256)])
english_alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

#def getCharset(enco):
#	allChars = ''.join(chr(i) for i in range(0x110000))
#	return allChars.encode(enco, errors='ignore').decode(enco)
#
#cp1252_alphabet = getCharset("cp1252")

def encryptVernam(m,k):
    kl = []
    c = ""
    for ck in k:
        kl.append(ord(ck))
    skl = len(kl)
    print("ascii llaves: {}".format(kl))
    for i,cm in enumerate(m):
    	print("{} kl[{}] = {}\t{} cm[] = {} \txOr = {}".format(chr(kl[i%skl]),i,kl[i%skl],cm,ord(cm), ord(cm) ^ kl[i%skl]))
    	c += chr(ord(cm) ^ kl[i%skl])
    return c;

def decryptVernam(c,k):
    return encryptVernam(c,k)

def encryptAtbash(m,a):
    #a = a.upper()
    #m = m.upper()
    na = a[::-1]
    c = ""
    for cm in m:
        for i, ca in enumerate(a):
            if cm == ca:
                c+=na[i]
    return c;

def decryptAtbash(c,a):
    return encryptAtbash(c,a)

def encryptVigenere(m,k,a):
    #a = a.upper()
    #k = k.upper()
    #m = m.upper()
    kl = []
    c = ""
    for ck in k:
        for i,ca in enumerate(a):
            if ck == ca:
                kl.append(i)
    skl = len(kl)
    sa = len(a)
    for i,cm in enumerate(m):
        for j,ca in enumerate(a):
            if cm == ca:
                c += a[(j+kl[i%skl])%sa]
    return c;

def decryptVigenere(c,k,a):
    #a = a.upper()
    #k = k.upper()
    #c = c.upper()
    kl = []
    m = ""
    for ck in k:
        for i,ca in enumerate(a):
            if ck == ca:
                kl.append(i)
    skl = len(kl)
    sa = len(a)
    for i,cm in enumerate(c):
        for j,ca in enumerate(a):
            if cm == ca:
                m += a[(j-kl[i%skl])%sa]
    return m;


def main():
	#variables
	alphabet = ""
	message = ""
	cryptogram = ""
	key = ""

	parser = argparse.ArgumentParser(description="Algoritmos de criptografía clasica (atbash - vernam - vigenere - proximamente <kasiski y enigma>, desarrollado por Gustavo Morán & Alix Cabrera.")

	# Se excluyen los 3 algoritmos, solo se puede utilizar uno al tiempo
	algorithmGroup = parser.add_mutually_exclusive_group(required = True)
	algorithmGroup.add_argument("-atb", "--atbash", action="store_true", help="set atbash algorithm to encrypt or decrypt")
	algorithmGroup.add_argument("-ver", "--vernam", action="store_true", help="set vernam algorithm to encrypt or decrypt")
	algorithmGroup.add_argument("-vig", "--vigenere", action="store_true", help="set vigenere algorithm to encrypt or decript")
	algorithmGroup.add_argument("-eni", "--enigma", action="store_true", help="set enigma algorithm to encrypt or decrypt")

	# Se excluye la entrada de la llave por texto y llave por archivo
	keyGroup = parser.add_mutually_exclusive_group()
	keyGroup.add_argument("-k", "--key", type=str, help="set a key")
	keyGroup.add_argument("-kF", "--keyFile", type=str, help="set a file to read the key")
	
	# Se exluyen el alfabeto con el alfabeto por archivo
	alphabetGroup = parser.add_mutually_exclusive_group()
	alphabetGroup.add_argument("-a", "--alphabet",type=str, help="set a custom alphabet")
	alphabetGroup.add_argument("-aF", "--alphabetFile", type=str, help="set a file to read an alphabet")

	# Bandera de la salida por archivo
	outputGroup = parser.add_mutually_exclusive_group()
	outputGroup.add_argument("-o", "--output", type=str, help="set the output file name")
	outputGroup.add_argument("-oF", "--outputFile", action="store_true", help="set the output file name with the input file name appending .dec or .cif (.dec when is used with -d flag, and -cif when is used with -e flag)" )
	
	# se excluyen las banderas de entrada de texto y entrada por archivo
	inputGroup = parser.add_mutually_exclusive_group(required = True)
	inputGroup.add_argument("-i", "--input", type=str, help="set a message to encrypt with -e flag, or a criptograma to decrypt with the -d flag")
	inputGroup.add_argument("-iF", "--inputFile",type=str, help="set an input file to read the message or the cryptograma, use -e to encrypt or -d to decrypt");
	
	# se excluyen las banderas para cifrar y decifrar
	encDecGroup = parser.add_mutually_exclusive_group(required=True)
	encDecGroup.add_argument("-e", "--encrypt", action="store_true", help="to encrypt the message")
	encDecGroup.add_argument("-d","--decrypt", action="store_true", help="to decrypt the cryptogram")
	
	args = parser.parse_args()

	# Tratamiento para el algoritmo Vernam
	if args.vernam:
#		if args.alphabet or args.alphabetFile:
#			print("The alphabet for vernam algorithm is in the message to encrypt/decript, is not necesary to set an alphabet")
#		else:
#			if args.key:
#        key = args.key
#    elif args.keyFile:
#        if path.exists(args.keyFile):
#            f = open(args.keyFile)
#            reading = f.read()
#            f.close()
#            key = reading[0:-1]
#        else:
#            print("Error: {} file dos not Exist or cannot been Found!".format(args.keyFile))
#            return

		if args.key:
			key = args.key
		elif args.keyFile:
			if path.exists(args.keyFile):
				f = open(args.keyFile)
				reading = f.read()
				f.close()
				key = reading[0:-1]
			else:
				print("Error: {} file dos not Exist or cannot been Found!".format(args.keyFile))
				return

		if args.input:
			if args.encrypt:
				message = args.input
			elif args.decrypt:
				cryptogram = args.input
		elif args.inputFile:
			reading=""
			if path.isfile(args.inputFile):
				f = open(args.inputFile, encoding="ISO-8859-1")
				reading = f.read()
				f.close()
				if args.encrypt:
					message = reading
				elif args.decrypt:
					cryptogram = reading
			else:
				print("Error: {} file does not Exist or cannot been Found!".format(args.inputFile))
				return
		
		if args.output:
			if path.exists(args.output):
				os.remove(args.output)
			f = open(args.output, "x", encoding="ISO-8859-1")
			if args.encrypt:
				cryptogram = encryptVernam(message,key)
				f.write(cryptogram)
				print ("message has been encrypted in file {}".format(args.output))
			elif args.decrypt:
				message = decryptVernam(cryptogram,key)
				f.write(message)
				f.close()
				print ("cryptogram has been decrypted in file {}".format(args.output))
		elif args.outputFile:
			if path.exists(args.inputFile):
				if args.encrypt:
					f = open(args.inputFile + ".cif", "x", encoding="ISO-8859-1")
					cryptogram = encryptVernam(message,key)
					f.write(cryptogram)
					print ("message has been encrypted in file {}".format(args.inputFile +".cif"))
				elif args.decrypt:
					f = open(args.inputFile + ".dec", "x", encoding="ISO-8859-1")
					message = decryptVernam(cryptogram,key)
					f.write(message)
					f.close()
					print ("cryptogram has been decrypted in file {}".format(args.inputFile +".dec"))
		else:
			if args.encrypt:
				cryptogram = encryptVernam(message,key)
				print ("message: {}".format(message))
				print ("cryptogram: {}".format(cryptogram))
			elif args.decrypt:
				message = decryptVernam(cryptogram,key)
				print ("cryptogram: {}".format(cryptogram))
				print ("message: {}".format(message))






	# Tratamiendo para el algoritmo Atbash
	elif args.atbash:
		if args.alphabet:
			alphabet = args.alphabet
		elif  args.alphabetFile:
			if path.exists(args.alphabetFile):
				f = open(args.alphabetFile, encoding="ISO-8859-1")
				reading = f.read()
				alphabet = reading[0:-1]
			else:
				print("Error: {} file does not Exist or cannot been Found!".format(args.alphabetFile))
				return
		else:
			print ("Alphabet by default (Spanish)")
			alphabet = spanish_alphabet

		
		if args.input:
			if args.encrypt:
				message = args.input
			elif args.decrypt:
				cryptogram = args.input
		elif args.inputFile:
			reading=""
			if path.isfile(args.inputFile):
				f = open(args.inputFile, encoding="ISO-8859-1")
				reading = f.read()
				if args.encrypt:
					message = reading
				elif args.decrypt:
					cryptogram = reading
			else:
				print("Error: {} file does not Exist or cannot been Found!".format(args.inputFile))
				return
		
		if args.output:
			if path.exists(args.output):
				os.remove(args.output)
			f = open(args.output, "x",encoding="ISO-8859-1")
			if args.encrypt:
				cryptogram = encryptAtbash(message,alphabet)
				f.write(cryptogram)
				#print ("alphabet : {}".format(alphabet))
				print ("message has been encrypted in file {}".format(args.output))
			elif args.decrypt:
				message = decryptAtbash(cryptogram,alphabet)
				f.write(message)
				#print ("alphabet : {}".format(alphabet))
				print ("cryptogram has been decrypted in file {}".format(args.output))
		elif args.outputFile:
			if path.exists(args.inputFile):
				if args.encrypt:
					f = open(args.inputFile + ".cif", "x", encoding="ISO-8859-1")
					cryptogram = encryptAtbash(message,alphabet)
					f.write(cryptogram)
					print ("message has been encrypted in file {}".format(args.inputFile +".cif"))
				elif args.decrypt:
					f = open(args.inputFile + ".dec", "x", encoding="ISO-8859-1")
					message = decryptAtbash(cryptogram,alphabet)
					f.write(message)
					f.close()
					print ("cryptogram has been decrypted in file {}".format(args.inputFile +".dec"))
		else:
			if args.encrypt:
				cryptogram = encryptAtbash(message,alphabet)
				#print ("alphabet : {}".format(alphabet))
				print ("cryptogram: {}".format(cryptogram))
			elif args.decrypt:
				message = decryptAtbash(cryptogram,alphabet)
				#print ("alphabet : {}".format(alphabet))
				print ("message: {}".format(message))

	# Tratamiento para el algoritmo Vigenere
	elif args.vigenere:
		#if args.key or args.keyFile:
		#	print("the vigenere algorithm doesn't need a key or a keyFile")
		#else:
		#	if args.alphabet:
		#		alphabet = args.alphabet
		#	elif  args.alphabetFile:
		#		if path.exists(args.alphabetFile):
		#			f = open(args.alphabetFile, encoding="ISO-8859-1")
		#			reading = f.read()
		#			alphabet = reading[0:-1]
		#		else:
		#			print("Error: {} file does not Exist or cannot been Found!".format(args.alphabetFile))
		#			return
		#	else:
		#		print("Alphabet its set to default!")
		if args.key:
			key = args.key
		elif args.keyFile:
			if path.exists(args.keyFile):
				f = open(args.keyFile, encoding="ISO-8859-1")
				reading = f.read()
				f.close()
				key = reading[0:-1]
			else:
				print("Error: {} file dos not Exist or cannot been Found!".format(args.keyFile))
				return

		if args.alphabet:
			alphabet = args.alphabet
		elif args.alphabetFile:
			if path.exists(args.alphabetFile):
				f = open(args.alphabetFile, encoding="ISO-8859-1")
				reading = f.read()
				f.close()
				alphabet = reading[0:-1]
			else:
				print("Error: {} file does not Exist or cannot been Found!".format(args.alphabetFile))
				return
		else:
			alphabet = spanish_alphabet


		if args.input:
			if args.encrypt:
				message = args.input
			elif args.decrypt:
				cryptogram = args.input
		elif args.inputFile:
			reading=""
			if path.isfile(args.inputFile):
				f = open(args.inputFile, encoding="ISO-8859-1")
				reading = f.read()
				f.close()
				if args.encrypt:
					message = reading
				elif args.decrypt:
					cryptogram = reading
			else:
				print("Error: {} file does not Exist or cannot been Found!".format(args.inputFile))
				return

		if args.output:
			if path.exists(args.output):
				os.remove(args.output)
			f = open(args.output, "x", encoding="ISO-8859-1")
			if args.encrypt:
				cryptogram = encryptVigenere(message,key,alphabet)
				f.write(cryptogram)
				print ("message has been encrypted in file {}".format(args.output))
			elif args.decrypt:
				message = decryptVigenere(cryptogram,key,alphabet)
				f.write(message)
				f.close()
				print ("cryptogram has been decrypted in file {}".format(args.output))
		elif args.outputFile:
			if path.exists(args.inputFile):
				if args.encrypt:
					f = open(args.inputFile + ".cif", "x", encoding="ISO-8859-1")
					cryptogram = encryptVigenere(message,key,alphabet)
					f.write(cryptogram)
					print ("message has been encrypted in file {}".format(args.inputFile +".cif"))
				elif args.decrypt:
					f = open(args.inputFile + ".dec", "x", encoding="ISO-8859-1")
					message = decryptVigenere(cryptogram,key,alphabet)
					f.write(message)
					f.close()
					print ("cryptogram has been decrypted in file {}".format(args.inputFile +".dec"))
		else:
			if args.encrypt:
				cryptogram = encryptVigenere(message,key,alphabet)
				print ("cryptogram : {}".format(cryptogram))
			elif args.decrypt:
				message = decryptVigenere(cryptogram,key,alphabet)
				print ("message : {}".format(message))

if __name__ == '__main__':
	startTime = time()
	main()
	finalTime = time()
	print ("Time : {}".format(finalTime - startTime))
