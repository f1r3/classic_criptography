#!/usr/bin/python3

import argparse
import os
import os.path as path
import hashlib
from functools import partial



english = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
spanish = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'Ñ', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']

conRotor1 = 0
conRotor2 = 0
conRotor3 = 0


#5 rotores de los cuales se eligen 3
#rotorI = ['E', 'K', 'M', 'F', 'L', 'G', 'D', 'Q', 'V', 'Z', 'N', 'T', 'O', 'W', 'Y', 'H', 'X', 'U', 'S', 'P', 'A', 'I', 'B', 'R', 'C', 'J']
rotorI = ['4', '10', '12', '5', '11', '6', '3', '16', '21', '25', '13', '19', '14', '22', '24', '7', '23', '20', '18', '15', '0', '8', '1', '17', '2', '9']
rotorIs = ['4', '10', '26', '12', '5', '11', '6', '3', '16', '21', '25', '13', '19', '14', '22', '24', '7', '23', '20', '18', '15', '0', '8', '1', '17', '2', '9']

#rotorII = ['A', 'J', 'D', 'K', 'S', 'I', 'R', 'U', 'X', 'B', 'L', 'H', 'W', 'T', 'M', 'C', 'Q', 'G', 'Z', 'N', 'P', 'Y', 'F', 'V', 'O', 'E']
rotorII = ['0', '9', '3', '10', '18', '8', '17', '20', '23', '1', '11', '7', '22', '19', '12', '2', '16', '6', '25', '13', '15', '24', '5', '21', '14', '4']
rotorIIs = ['0', '9', '3', '10', '18', '8', '17', '20', '23', '1', '11', '7', '22', '19', '12', '2', '16', '6', '25', '13', '15', '24', '5', '21', '14', '26', '4']

#rotorIII = ['B', 'D', 'F', 'H', 'J', 'L', 'C', 'P', 'R', 'T', 'X', 'V', 'Z', 'N', 'Y', 'E', 'I', 'W', 'G', 'A', 'K', 'M', 'U', 'S', 'Q', 'O']
rotorIII = ['1', '3', '5', '7', '9', '11', '2', '15', '17', '19', '23', '21', '25', '13', '24', '4', '8', '22', '6', '0', '10', '12', '20', '18', '16', '14']
rotorIIIs = ['1', '3', '5', '7', '9', '11', '2', '15', '17', '19', '23', '21', '25', '13', '24', '4', '8', '22', '6', '26', '0', '10', '12', '20', '18', '16', '14']

#rotorIV = ['E', 'S', 'O', 'V', 'P', 'Z', 'J', 'A', 'Y', 'Q', 'U', 'I', 'R', 'H', 'X', 'L', 'N', 'F', 'T', 'G', 'K', 'D', 'C', 'M', 'W', 'B']
rotorIV = ['4', '18', '14', '21', '15', '25', '9', '0', '24', '16', '20', '8', '17', '7', '23', '11', '13', '5', '19', '6', '10', '3', '2', '12', '22', '1']
rotorIVs = ['4', '18', '14', '26', '21', '15', '25', '9', '0', '24', '16', '20', '8', '17', '7', '23', '11', '13', '5', '19', '6', '10', '3', '2', '12', '22', '1']

#rotorV = ['V', 'Z', 'B', 'R', 'G', 'I', 'T', 'Y', 'U', 'P', 'S', 'D', 'N', 'H', 'L', 'X', 'A', 'W', 'M', 'J', 'Q', 'O', 'F', 'E', 'C', 'K']
rotorV = ['21', '25', '1', '17', '6', '8', '19', '24', '20', '15', '18', '3', '13', '7', '11', '23', '0', '22', '12', '9', '16', '14', '5', '4', '2', '10']
rotorVs = ['21', '25', '1', '17', '6', '8', '19', '24', '20', '26', '15', '18', '3', '13', '7', '11', '23', '0', '22', '12', '9', '16', '14', '5', '4', '2', '10']


#reflectorB = ['Y', 'R', 'U', 'H', 'Q', 'S', 'L', 'D', 'P', 'X', 'N', 'G', 'O', 'K', 'M', 'I', 'E', 'B', 'F', 'Z', 'C', 'W', 'V', 'J', 'A', 'T']
reflectorB = ['24', '17', '20', '7', '16', '18', '11', '3', '15', '23', '13', '6', '14', '10', '12', '8', '4', '1', '5', '25', '2', '22', '21', '9', '0', '19']
reflectorBs = ['25', '18', '21', '7', '17', '19', '11', '3', '16', '24', '14', '6', '15', '13', '10', '12', '8', '4', '1', '5', '26', '2', '23', '22', '9', '0', '20']


#rotores elegidos
lisRotores = [rotorI, rotorII, rotorIII, rotorIV, rotorV]
lisRotoresS = [rotorIs, rotorIIs, rotorIIIs, rotorIVs, rotorVs]
rotores = [rotorI, rotorII, rotorIII]

#parejas de letras elegidas para intercambiar
    
par1 = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
par2 = ['16', '17', '18', '19', '20', '21', '22', '23', '24', '25']
    
def girarRotor1(rotor):
    aux = rotor[0]
    rotor.remove(rotor[0])
    rotor.append(aux)
    global conRotor1
    conRotor1 +=1
    tam = len(rotor)
    if conRotor1 == tam:
        girarRotor2(rotores[0])
        conRotor1 = 0

def girarRotor2(rotor):
    aux = rotor[0]
    rotor.remove(rotor[0])
    rotor.append(aux)
    global conRotor2
    conRotor2 += 1
    tam = len(rotor)
    if conRotor2 == tam:
        girarRotor2(rotores[1])
        conRotor2 = 0

def girarRotor3(rotor):
    aux = rotor[0]
    rotor.remove(rotor[0])
    rotor.append(aux)
    global conRotor3
    conRotor3 += 1
    tam = len(rotor)
    if conRotor3 == tam:
        girarRotor3(rotores[2])
        conRotor3 = 0

def md5sum(filename):
    with open(filename, mode='rb') as f:
        d = hashlib.md5()
        for buf in iter(partial(f.read, 1024), b''):
            d.update(buf)
    return d.hexdigest()



def rotorLetra(letra): #pasa la letra por los rotores, falta intercambiar con su pareja, si existe
    letra = rotores[0][letra] #letra del r1
    letra = rotores[1][int(letra)] #letra del r2
    letra = rotores[2][int(letra)] #letra del r3
    letra = reflectorB[int(letra)]
    
    #ahora toca en reversa
    letra = rotores[2].index(str(letra)) #pos de la letra del r3
    letra = rotores[1].index(str(letra)) #pos de la letra del r2
    letra = rotores[0].index(str(letra)) #pos de la letra del r1
    return letra

def intercambiarLetra(letra): #Mira si la letra está en las parejas seleccionadas, y si lo está, la intercambia con su pareja
    par = letra
    esta = par1.count(letra)
    if esta > 0:
        pos = par1.index(letra)
        par = par2[pos]
    elif par2.count(letra) > 0:
        pos = par2.index(letra)
        par = par1[pos]
    return par

def cifrarLetra(letra, abc):
    letra = abc.index(letra) #index de la letra en el abc
    letra = str(letra)
    letra = intercambiarLetra(letra)
    letra = int(letra)
    letra = rotorLetra(letra)
    girarRotor1(rotores[0])
    letra = str(letra)
    letra = intercambiarLetra(letra)
    
    letra = abc[int(letra)]
    return letra

def encryptEnigma(mensaje, abc):
    mensaje = mensaje.upper()
    cifrado = ''
    tam = 0
    tam = len(mensaje)

    for i in range(tam):
        letra = mensaje[i] #index de la letra en el abc
        if (abc.count(letra) == 1):
            l = cifrarLetra(letra, abc)
        else:
            l = letra
        cifrado += l
    return cifrado

def validarRotores(rotores):
    valido = True
    if(rotores < 556):
        if rotores%10 < 6 and rotores %10 >0:
            rot = rotores // 10
            if rot%10 < 6 and rot %10 >0:
                rot = rot // 10
                if rot < 6 and rot >0:
                    valido = True
                else:
                    valido = False
            else:
                valido = False
        else:
            valido = False
    else:
        valido = False
    return valido

def validarInicios(inicios, abc):
    aux = len(abc)
    if inicios%100 < aux and inicios%100 >= 0:
        ini = inicios // 100
        if ini%100 < aux and ini%100 >=0:
            if ini//100 < aux and ini%100 >=0:
                valido = True
            else:
                valido = False
        else:
            valido = False
    else:
        valido = False
    return valido

def validarLetras(letras, abc):
    letras.upper()
    lisLetras =[]
    valido = False
    tam = len(letras)
    for i in range(tam):
        lisLetras.append(letras[i])
    tam = len(lisLetras)
    if(tam == 20):
        for i in range(tam):
            l = lisLetras[i]
            if (lisLetras.count(l)==1 and abc.count(l)==1):
                valido = True
            else:
                valido = False
                break
    else:
        valido = False
    return valido


def iniciarR1(rotor, start):
    while conRotor1 < start:
        girarRotor1(rotor)
            
def iniciarR2(rotor, start):
    while conRotor2 < start:
        girarRotor2(rotor)
        
def iniciarR3(rotor, start):
    while conRotor3 < start:
        girarRotor3(rotor)


def main():
    
    #mensaje en claro
    message = ""
    cryptogram = ""
    alfabeto = english
    
    parser = argparse.ArgumentParser(description="Enigma Machine: Series of rotor cipher machines developed and used in the early- to mid-20th century to protect commercial, diplomatic and military communication. Invented by Arthur Scherbius at the end of World War I. Early models were used commercially, and adopted by military and government services of several countries, most notably Nazi Germany before and during World War II. Several different Enigma models were produced, but the German military models, having a plugboard to exchange 10 pairs of letters were the most complex..  Repository https://gitlab.com/tavomoran/classic_criptography This program has not warranty, use under your own risk. Script developed by Gustavo Móran - Alix Estefany Cabrera for the Introduction to Cryptography class. Teacher: Siler Amador")
    inputGroup = parser.add_mutually_exclusive_group(required = True)
    inputGroup.add_argument("-i", "--input", type=str, help="Set a message to encrypt with -e flag, or a Cryptogram to decrypt with the -d flag")
    inputGroup.add_argument("-iF", "--inputFile",type=str, help="set an input file to read the message or the Cryptogram, use -e to encrypt or -d to decrypt"); 
    encDecGroup = parser.add_mutually_exclusive_group(required=True)
    encDecGroup.add_argument("-e", "--encrypt", action="store_true", help="to encrypt the message")
    encDecGroup.add_argument("-d","--decrypt", action="store_true", help="to decrypt the cryptogram")
    outputGroup = parser.add_mutually_exclusive_group()
    outputGroup.add_argument("-o", "--output", type=str, help="Set the output file name. WARNING: this will overwrite the file")
    alphabetGroup = parser.add_mutually_exclusive_group()
    alphabetGroup.add_argument("-a", "--alphabet", type=int, choices=[1,2], default=1,help="set an alphabet [1] Spanish, [2] English")

    plugboardGroup = parser.add_mutually_exclusive_group()
    plugboardGroup.add_argument("-p", "--plugboard", type = str, help="Set the 10 pairs of letters to be exchanged with the plugboard. Enter 20 letters to make the pairs. Example: AJDUIRKWSOQXZHBFPLMN, A will be exchanged with J, and J with A. D will be exchanged with U....")
    rotorGroup = parser.add_mutually_exclusive_group()
    rotorGroup.add_argument("-r", "--rotors", type = int, default = 123, help="Select 3 rotors from rotors 1 to five, enter the 3 numbers without spaces. Example = 325 will select rotors 3, 2 and 5 int that order. You can repeat and choose the same rotor more than once. For example: 111")
    rotorStartGroup = parser.add_mutually_exclusive_group()
    rotorStartGroup.add_argument("-rs", "--rotorStart", type = int, default = 000000, help="Enter the start position of each rotor. Example= 200010 starts rotor 1 at the 20th position, rotor 2 at the 00 position and rotor 3 at the 10th position")

    args = parser.parse_args()


    if args.alphabet:
        if args.alphabet == 1:
            alfabeto.clear()
            tam = len(spanish)
            for i in range(tam):
                alfabeto.append(spanish[i])
            
            lisRotores.clear()
            tam = len(lisRotoresS)
            for i in range(tam):
                lisRotores.append(lis-rotoresS[i])
            reflectorB.clear()
            tam = len(reflectorBs)
            for i in range(tam):
                reflectorB.append(reflectorBs[i])
        elif args.alphabet == 2:
            alfabeto.clear()
            tam = len(english)
            for i in range(tam):
                alfabeto.append(english[i])

    if args.input:
        if args.encrypt:
            message = args.input
        elif args.decrypt:
            cryptogram = args.input
    elif args.inputFile:
        reading=""
        if path.isfile(args.inputFile):
            f = open(args.inputFile)
            reading = f.read()
            f.close()
            print(md5sum(args.inputFile))
            if args.encrypt:
                
                message = reading
            elif args.decrypt:
                cryptogram = reading
        else:
            print("Error: {} file does not Exist or cannot be Found!".format(args.inputFile))
            return
        
    

    if args.plugboard:
        if validarLetras(args.plugboard, alfabeto):
            tam = len(args.plugboard)
            par1.clear()
            par2.clear()
            for i in range(tam):
                if(i%2 == 0):
                    par2.append(args.plugboard[i])
                    
                else:
                    par1.append(args.plugboard[i])  
        else:
            print("Error: {} invalid letters. Enter exactly 20 letters. You can't repeat letetrs or use special characters".format(args.plugboard))
            return

    
    if args.rotors:
        if validarRotores(args.rotors):
            if (alfabeto == spanish):
                lisRotores.clear()
                tam = len(lisRotoresS)
                for i in range(tam):
                    lisRotores.append(lis-rotoresS[i])
                reflectorB.clear()
                tam = len(reflectorBs)
                for i in range(tam):
                    reflectorB.append(reflectorBs[i])
            else:
                lisRotores
            rotores.clear()
            aux = args.rotors // 100 #primera cifra del numero
            rotores.append(lisRotores[int(aux-1)])
            aux = args.rotors
            aux = aux // 10
            aux = aux%10 #segunda cifra
            rotores.append(lisRotores[aux-1])
            aux = args.rotors
            aux = aux % 100
            aux = aux % 10
            rotores.append(lisRotores[aux-1])
        else:
            print("Error: {} invalid rotors".format(args.rotors))
            return


    if args.rotorStart:
        if validarInicios(args.rotorStart, alfabeto)==True:
            if args.rotorStart > 000000:
                aux = args.rotorStart // 10000 #primeras 2 cifras del numero
                iniciarR1(rotores[0], aux)
                aux = args.rotorStart // 100
                aux = aux % 100
                iniciarR2(rotores[1], aux)
                aux = args.rotorStart % 100
                iniciarR3(rotores[2], aux)
            rotores.append(args.rotors%10)
        else:
            print("Error: {} invalid start for rotors".format(args.rotorStart))
            return

    if args.output:
        if path.exists(args.output):
            os.remove(args.output)
        f = open(args.output, "x")
        if args.encrypt:
            cryptogram = encryptEnigma(message, alfabeto)
            f.write(cryptogram)
            print(md5sum(args.output))
            print ('message has been encrypted in file {}'.format(args.output))
        elif args.decrypt:
            message = encryptEnigma(cryptogram, alfabeto)
            f.write(message)
            f.close()
            
            print(md5sum(args.output))
            print ("cryptogram has been decrypted in file {}".format(args.output))
    else:
        if args.encrypt:
            cryptogram = encryptEnigma(message, alfabeto)
            print ("message : {}".format(message))
            print ("cryptogram : {}".format(cryptogram))
        elif args.decrypt:
            message = encryptEnigma(cryptogram, alfabeto)
            print ("cryptogram : {}".format(cryptogram))
            print ("message : {}".format(message))
if __name__ == '__main__':
    main()
    
        


            
